import json

from django.forms import model_to_dict
from django.http import HttpResponse

from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer

from repoapp.serializes import UserSerializer, RepositoriesSerializer
from repoapp import models
from helper import get_repository, check_missing_parameters


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs["content_type"] = "application/json"
        super(JSONResponse, self).__init__(content, **kwargs)

def return_response(message="", status="success"):
    return {"message": message, "status": status}

@csrf_exempt
def get_users(request):
    # Get all users
    data = models.user.get_all_users()
    user_serializer = UserSerializer(data, many=True)
    return JSONResponse(user_serializer.data)

@csrf_exempt
def repositories(request):

    if request.method == "GET":
        # Get all repositories
        data = models.repositories.get_repositories()

        # Serializer repositories data
        repository_serializer = RepositoriesSerializer(data, many=True)
        response_data = repository_serializer.data

    elif request.method == "POST":
        data = json.loads(request.body)

        # Check if any parameter in missing
        check_parameters = check_missing_parameters(data)
        if isinstance(check_parameters, str):
            return JSONResponse(return_response(message=check_parameters, status="error"))

        # Get user by id, if does not exist return error
        user_obj = models.user.get_user_by_id(user_id=data["user_id"])
        if not user_obj:
            return JSONResponse(return_response(message="Bad user id", status="error"))

        # Create new repository
        new_repository = models.repositories.add_repositories(
            username=user_obj, name=data["name"], description=data["description"], url=data["url"],
            language=data["language"], watchers_count=data["watchers_count"])

        response_data = model_to_dict(new_repository)

    elif request.method in "PUT":
        data = json.loads(request.body)

        # Check if repository exist
        repository = get_repository(data)
        if isinstance(repository, str):
            return HttpResponse(json.dumps(repository))

        data.pop("repository_id")

        # Edited all send parameters for repository and refresh data after save
        edited_repository = models.repositories.edit_repository(repository, **data)
        edited_repository.refresh_from_db()

        # Get dict from repository object
        response_data = model_to_dict(edited_repository)
    else:
        response_data = return_response(message="GET, POST or PUT method need to be send", status="error")

    return JSONResponse(response_data)

@csrf_exempt
def delete_repository(request, pk):
    # Delete repository for given pk
    if request.method == "DELETE":
        repository = get_repository({"repository_id": int(pk)})

        # If response from get_repository is string return error message
        if isinstance(repository, str):
            return JSONResponse(return_response(message=repository, status="error"))

        # Delete repository and return success
        models.repositories.delete_repository(repository)
        return JSONResponse((return_response()))
    else:
        return JSONResponse(return_response(message="Bad request type", status="error"))


@csrf_exempt
def get_repositories_for_username(request, username):
    # Get user by username and get all repositories for that user
    user = models.user.get_user_by_name(username)
    if not user:
            return JSONResponse(return_response(message="Bad user id", status="error"))
    data = models.repositories.get_repositories_for_username(owner=user)

    # Serializer repositories data
    repository_serializer = RepositoriesSerializer(data, many=True)
    return JSONResponse(repository_serializer.data)
