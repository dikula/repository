__author__ = "dikula"


from rest_framework import serializers
from repoapp.models.models import User, Repositories


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "username", "avatar")
        
        
class RepositoriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Repositories
        fields = ("id", "owner", "name", "description", "url", "language", "watchers_count")