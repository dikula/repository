from django.core.management import BaseCommand

__author__ = 'dikula'

import urllib2
import simplejson as simplejson

from repoapp import models


class Command(BaseCommand):

    def handle(self, *args, **options):
        users = ["GrahamCampbell", "fabpot"]
        for user in users:
            # Check if data is already imported
            data_exist = models.user.get_user_by_name(username=user)

            # If data already exist for given user then skip
            if data_exist is None:
                # Get info from url
                url = "https://api.github.com/users/{}/repos".format(user)
                req = urllib2.Request(url, None, {'user-agent':'syncstream/vimeo'})
                opener = urllib2.build_opener()
                f = opener.open(req)
                json = f.read()
                repo_data = simplejson.loads(json)

                # Create new user
                username = models.user.add_new_user(username=repo_data[0]["owner"]["login"],
                                                    avatar=repo_data[0]["owner"]["avatar_url"])

                # Create repositories for user
                for data in repo_data:
                    models.repositories.add_repositories(username=username, name=data["name"],
                                                         description=data["description"], url=data["git_url"],
                                                         language=data["language"], watchers_count=data["watchers_count"] )

        print 'Data successfully imported'
