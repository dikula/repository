import os
import urllib2
import simplejson as simplejson
import django

__author__ = 'dikula'

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "repo.settings")
django.setup()
from repoapp import models


# def import_repo_from_url():#url):
if __name__ == '__main__':
    urls = ["https://api.github.com/users/GrahamCampbell/repos"]
    for url in urls:
        # Check if data is already imported
        data_exist = models.user.get_user_by_name(username="GrahamCampbell")

        # if data_exist is None:
        req = urllib2.Request(url, None, {'user-agent':'syncstream/vimeo'})
        opener = urllib2.build_opener()
        f = opener.open(req)
        json = f.read()
        repo_data = simplejson.loads(json)
        username = models.user.add_new_user(username=repo_data[0]["owner"]["login"],
                                            avatar=repo_data[0]["owner"]["avatar_url"])

        for data in repo_data:
            models.repositories.add_repositories(username=username, name=data["name"],
                                                 description=data["description"], url=data["git_url"],
                                                 language=data["language"], watchers_count=data["watchers_count"] )
