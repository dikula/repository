from __future__ import unicode_literals

from django.db import models

# Create your models here.


class User(models.Model):
    username = models.CharField(max_length=50, unique=True)
    avatar = models.SlugField(max_length=100)


class Repositories(models.Model):
    owner = models.ForeignKey(User, related_name='repo_owner', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200, blank=True, null=True)
    url = models.URLField(max_length=200)
    language = models.CharField(max_length=20, blank=True, null=True)
    watchers_count = models.IntegerField()

