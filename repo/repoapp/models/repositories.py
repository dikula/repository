__author__ = 'dikula'

from models import Repositories

def add_repositories(username, name, description, url, language, watchers_count):
    # Saves new repository and return new create object
    new_repo = Repositories(owner=username, name=name, description=description, url=url, language=language,
                            watchers_count=watchers_count)
    new_repo.save()
    return new_repo

def edit_repository(repository, **kwargs):
    # Edit repository for given id and parameters
    for value, attr in kwargs.iteritems():
        setattr(repository, value, attr)
    repository.save()
    return repository

def get_repository_by_id(repository_id):
    # Return list of all repositories for given username object
    return Repositories.objects.filter(id=repository_id).first()

def get_repositories():
    # Return all repositories
    return Repositories.objects

def delete_repository(repository):
    # Delete repository
    repository.delete()

def get_repositories_for_username(owner):
    # Return all repositories for given owner
    return Repositories.objects.filter(owner=owner)
