__author__ = 'dikula'


from models import User

def add_new_user(username, avatar):
    # Create new user
    new_user = User(username=username, avatar=avatar)
    new_user.save()
    return new_user

def get_user_by_id(user_id):
    # Return user for given user_id
    return User.objects.filter(id=user_id).first()

def get_all_users():
    # Return all users
    return User.objects

def get_user_by_name(username):
    # Return user for given name
    return User.objects.filter(username=username).first()
