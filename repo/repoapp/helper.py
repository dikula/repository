__author__ = 'dikula'

from repoapp import models

def get_repository(data):
    # Get user from db if exist
    if "repository_id" not in data:
        return "Repository id is not send"

    # Check if repository_id is integer
    repository_id = data["repository_id"]
    if not isinstance(repository_id, int):
        return "Repository id need to be integer"

    # Check if repository exist in database
    repository = models.repositories.get_repository_by_id(repository_id=repository_id)
    if not repository:
        return "Bad repository id"

    return repository


def check_missing_parameters(data):
    # Create list of all needed parameters for creating new repository
    repository_keys = ["name", "description", "url", "language", "watchers_count", "user_id"]

    # Find if any parameter is missing and return error if it is
    missing_keys = [x for x in repository_keys if x not in data.keys()]
    if len(missing_keys):
        return "Missing parameters {}".format(missing_keys)

